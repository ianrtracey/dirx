Prerequisites:
====================
1. Install Neo4j (http://neo4j.com/download/) and specify v2.2.2 in conf/neo4j-server.properties
2. Install Maven (https://maven.apache.org/download.cgi)
3. Download Neo4j Batch Importer (https://github.com/jexp/batch-import)
    3a. Specify db path and sample data in import-mvn.sh
    3b. run 'sh import-mvn.sh'


* Notes:
    * Make sure that database specified by the Neo4j Batch Importer is the same as specified in conf/neo4j-server.properties
    * Ensure Neo4j is running when you run batch import and restart the server after the import
    * You may receive warnings after import, you may ignore these
    * Check the file paths of your batch.properties inside of Batch Import
    * Make sure batch_import.csv.delim=, is uncommented in batch.properties if using CSV
    * Your security/SecurityConfig.java will need to change based on your login credentials

Quick Start:
====================
1. Start Neo4j server -- 'bin/neo4j start'
2. Run Spring Boot app -- 'mvn spring-boot:run -Drun.jvmArguments="-Dusername=yourUsernameHere -Dpassword=yourPasswordHere"
3. Go to localhost:8080
4. Enter 'user' as username and 'password' as password at login prompt
5. You can also view Neo4j nodes at localhost:7474

Introduction
====================

A project to build a next generation directory **web application** that is mobile-ready, responsive, and 
focused on the relationships of a typical **_business network_**. 

* People
* Projects
* Teams
* Places

We plan to use a graph database (Neo4j) to store the objects and relationships, 
so the system can scale to literally billions of nodes and connections. 

Key features
-----------

* Search 
    * Auto-completion for userid/username
	* Soundex matching
	* Project name and description search
* Dashboard that shows the currently logged in users connections.
    * People: Current, Peers, Reports (if present), Manager
	* Places: Current, Favorites
	* Teams: active teams
	* Projects: active projects, link to the archives.
* Web Service REST API
	* Views of the data objects
	* Updating and adding new network objects.
* Detail Views
	* Details of a person
	* Details for a group of people
	* Details of a Project, Place, Team
* Browsing
	* Visualization (ala D3JS.org) of a group / team
	* Projects rolled up into larger programs/initiatives
	* Matrix reporting structures