package org.directoryx.service;

import org.directoryx.domain.Person;
import org.directoryx.domain.Project;
import org.directoryx.repository.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

/**
 * Created by svarjabe on 6/22/15.
 */

@Service("projectService")
public class ProjectServiceImpl extends GenericService<Project> implements ProjectService{

    @Autowired
    private ProjectRepository projectRepository;

    @Override
    public ProjectRepository getRepository() { return projectRepository; }

    public Project saveProject(Project project) { return projectRepository.save(project); }

    public Project findByProjectId(String projectId) { return projectRepository.findByProjectId(projectId); }

    public Project setLead(Project project, String userId) { return projectRepository.setLead(project.getName(), userId); }

    public ArrayList<Person> getMembers(Project project) { return projectRepository.getMembers(project.getProjectId()); }

    public Person getLead(Project project) { return projectRepository.getLead(project.getProjectId()); }


}
