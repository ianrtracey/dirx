package org.directoryx.service;

import org.directoryx.domain.Person;
import org.directoryx.domain.Project;

import java.util.ArrayList;


public interface ProjectService extends Service<Project> {

    public Project saveProject(Project project);

    public Project findByProjectId(String projectId);

    public Project setLead(Project project, String userId);

    public ArrayList<Person> getMembers(Project project);

    public Person getLead(Project project);




}
