package org.directoryx.service;

import org.directoryx.domain.Person;
import org.directoryx.domain.PersonWithHash;
import java.util.ArrayList;
import java.util.Collection;
import org.neo4j.rest.graphdb.query.CypherTransaction.Result;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public interface PersonService extends Service<Person>{

    public Person findByUserId(String userId);

    public ArrayList<Person> getReportingStructure(String userId);

    public ArrayList<Person> findByFirstName(String firstName);
    public ArrayList<Person> findByFirstAndLastName(String firstName, String lastName);
        public ArrayList<Person> findContractorByFirstName(String firstName);
        public ArrayList<Person> findEmployeeByFirstName(String firstName);
    public Collection<PersonWithHash> getManagers(String userId);

    Collection<PersonWithHash> getReportees(String userId);

    public ArrayList<Person> search(String term);
    public Person favorite(String userId);

    public Person savePerson(Person person);

    public ArrayList<Person> getFavorites(String userId);

    public Void removeFavorite(String userId);

    public int countByFirstName(String term);

    public Result getReportingChain();



}
