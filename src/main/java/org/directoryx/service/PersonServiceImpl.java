package org.directoryx.service;

import org.apache.log4j.Logger;
import org.directoryx.Application;
import org.directoryx.domain.Person;
import org.directoryx.domain.PersonWithHash;
import org.directoryx.repository.PersonRepository;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.neo4j.rest.graphdb.query.CypherTransaction.Result;
import org.springframework.stereotype.Service;

import java.util.*;

@Service("personService")
public class PersonServiceImpl extends GenericService<Person> implements PersonService {

    static Logger log = Logger.getLogger(Application.class.getName());

    @Autowired
    private PersonRepository personRepository;

    @Override
    public GraphRepository<Person> getRepository() {
        return personRepository;
    }

    public Person findByUserId(String userId) {
        return personRepository.findByUserId(userId);
    }

    public ArrayList<Person> getReportingStructure(String userId) {
        return personRepository.findReportingStructure(userId);
    }

    public Collection<PersonWithHash> getManagers(String userId) {
        return personRepository.getManagers(userId);
    }
    
    public Collection<PersonWithHash> getReportees(String userId) {
        return personRepository.getReportees(userId);
    }

    public ArrayList<Person> searchByUserId(String userId) { return personRepository.searchByUserId(userId); }
    public ArrayList<Person> findByLastName(String lastName) { return personRepository.findByLastName(lastName); }
    public ArrayList<Person> findByFirstName(String firstName) {
        return personRepository.findByFirstName(firstName);
    }
    public ArrayList<Person> findByFirstAndLastName(String firstName, String lastName) { return personRepository.findByFirstAndLastName(firstName, lastName); }
        public ArrayList<Person> findContractorByFirstName(String firstName) { return personRepository.findContractorByFirstName(firstName); }

        public ArrayList<Person> findEmployeeByFirstName(String firstName) { return personRepository.findEmployeeByFirstName(firstName); }


    public Person favorite(String userId) { return personRepository.favorite(userId); }

    public Person savePerson(Person person) { return personRepository.save(person); }

    public ArrayList<Person> getFavorites(String userId) { return personRepository.getFavorites(userId); }
    public Void removeFavorite(String userId) {  return personRepository.removeFavorite(userId); }

    public int countByFirstName(String term){ return personRepository.countByFirstName(term); }

    public ArrayList<Person> search(String term) {

        ArrayList<Person> people;
        String firstName, lastName;
        String[] parsedTerms;
        StringBuilder builder = new StringBuilder();

        log.info("searching for " + term + "...");

        parsedTerms = parseTerm(term);

//        for(int i = 0; i < parsedTerms.length; i++) {
//            parsedTerms[i] = builder.append("(?i)").append(parsedTerms[i]).append(".*");
//        }
        if(parsedTerms.length == 2) {
            firstName = "(?i)" + term.substring(0, term.indexOf(" ")) + ".*";
            lastName = "(?i)" + term.substring(term.indexOf(" ") + 1) + ".*";
            people = findByFirstAndLastName(firstName, lastName);
        } else {
            people = findByFirstName("(?i)" + term + ".*");

            if(people.size() == 0) {
                people = findByLastName("(?i)" + term + ".*");
            }
        }

        if(people.size() == 0) {
            people = searchByUserId(term + ".*");
        }

        log.info("found " + people.size() + " person/people.");
        return people;
    }

    private String[] parseTerm(String term) {
        return term.split("\\s+");
    }

    public Result getReportingChain() {
        return personRepository.getReportingChain();
    }
}