package org.directoryx.repository;

import org.directoryx.domain.Person;
import org.directoryx.domain.PersonWithHash;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.stereotype.Repository;

import org.neo4j.rest.graphdb.query.CypherTransaction.Result;
import java.util.ArrayList;
import java.util.Collection;

import java.util.HashMap;

import java.util.Map;

@Repository
public interface PersonRepository extends GraphRepository<Person> {


    @Query("MATCH (n:Person ) WHERE n.userId={0} RETURN n;")
    Person findByUserId(String userId);

    @Query("MATCH (n:Person ) WHERE n.userId=~ {0} RETURN n;")
    ArrayList<Person> searchByUserId(String userId);

    /* Find by first name and return in ModelAndView */
    @Query("MATCH (n:Person ) WHERE n.firstName =~ {0} RETURN n LIMIT 150;")
    ArrayList<Person> findByFirstName(String firstName);

    @Query("MATCH (n:Person ) WHERE n.lastName =~ {0} RETURN n LIMIT 150;")
    ArrayList<Person> findByLastName(String firstName);

    @Query("MATCH (n:Person ) WHERE n.firstName =~ {0} AND n.lastName =~ {1} RETURN n LIMIT 150;")
    ArrayList<Person> findByFirstAndLastName(String firstName, String lastName);

    @Query("MATCH (n:Person ) WHERE n.firstName =~ {0} RETURN COUNT(n);")
    int countByFirstName(String firstName);

    /* Handles the AJAX call to searching first names by employee status */
    @Query("MATCH (n:Person {status: 'Employee'}) WHERE n.firstName =~ {0} RETURN n LIMIT 150;")
    ArrayList<Person> findEmployeeByFirstName(String firstName);

    /* Handles the AJAX call to searching first names by contractor status */
    @Query("MATCH (n:Person {status: 'Contractor'}) WHERE n.firstName =~ {0} RETURN n LIMIT 150;")
    ArrayList<Person> findContractorByFirstName(String firstName);

    @Query("MATCH (starter:Person {userId: {0}})<-[r:WORKS_FOR]-(n:Person) OPTIONAL MATCH (n)<-[q:WORKS_FOR]-(p:Person) RETURN n as person, count(q) as reportCounts")
    Collection<PersonWithHash> getReportees(String userId);

    @Query("MATCH (n:Person {userId: 'sarippi'})-[:WORKS_FOR*..]->(manager:Person)<-[r:WORKS_FOR]-(report:Person) RETURN manager as person, count(DISTINCT r) + 1 as reportCounts;")
    Collection<PersonWithHash> getManagers(String userId);

    @Query("MATCH (starter:Person {userId: {0}})<-[r:WORKS_FOR]-(n:Person) OPTIONAL MATCH (n)<-[q:WORKS_FOR]-(p:Person) WITH n AS pe, count(q) as c RETURN c")
    ArrayList<Integer> getReporteesCount(String userId);


    @Query("MATCH (n:Person) RETURN n;")
    ArrayList<Person> findAllPeople();

    @Query("MATCH (n:Person {firstName: {0}}) RETURN n;")
    ArrayList<Person> findByExactFirstName(String firstName);

    @Query("MATCH (n) DELETE n;")
    void deleteData();

    @Query("LOAD CSV WITH HEADERS FROM \"file:/code/directoryx/data/directoryx_sample2.csv\" AS line " +
            "CREATE (person:Person {personId: line.id, firstName: line.first_name, lastName: line.last_name, " +
            "userId: line.userid, title: line.title, status: line.status, photo: line.photo, managerId: line.managerId, " +
            "city: line.city, country: line.country, org: line.org, phone: line.phone, email: line.email});")
    void loadData();

    @Query("CREATE INDEX ON :Person(managerId);")
    void createManagerIdIndex();
    @Query("CREATE INDEX ON :Person(userId);")
    void createUserIdIndex();
    @Query("CREATE INDEX ON :Person(firstName);")
    void createFirstNameIndex();

    // @TODO make relationship without managerId
    @Query("MATCH (n:Person),(m:Person) WHERE n.userId=m.managerId CREATE (m)-[:WORKS_FOR]->(n);")
    void makeRelationships();

    //way of searching for reporting structure of person
    @Query("MATCH (n:Person {userId: {0}})-[:WORKS_FOR*1..2]->(manager:Person)," +
            "(n:Person {userId: {0}})<-[:WORKS_FOR]-(reportee:Person) RETURN reportee, n, manager;")
    ArrayList<Person> findReportingStructure(String userId);

    @Query("MATCH (n:Person {userId: {0}})-[:WORKS_FOR*1..2]->(manager:Person)" +
            "MATCH (n:Person {userId: {0}})<-[:WORKS_FOR]-(reportee:Person) RETURN reportee;")
    ArrayList<Person> findReportingSnapshot(String userId);

    @Query("MATCH (a:Person {userId: \"iatracey\"}),(b:Person) WHERE b.userId = {0} CREATE (a)-[r:FAVORITES]->(b) RETURN b")
    Person favorite(String userId);

    @Query("MATCH (n:Person {userId: {0}})-[:FAVORITES]->(favorites:Person) RETURN favorites;")
    ArrayList<Person> getFavorites(String userId);

    @Query("MATCH (a:Person {userId: \"iatracey\"}),(b:Person) WHERE b.userId = {0} MATCH (a)-[rel:FAVORITES]->(b) DELETE rel;")
    Void removeFavorite(String userId);

    @Query("MATCH (starter:Person {userId: 'shamree'})<-[r:WORKS_FOR]-(n:Person) OPTIONAL MATCH (n)<-[q:WORKS_FOR]-(p:Person) WITH n as person, count(q) as count RETURN person,count")
    Result getReportingChain();
}
