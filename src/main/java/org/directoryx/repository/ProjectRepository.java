package org.directoryx.repository;

import org.directoryx.domain.Person;
import org.directoryx.domain.Project;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public interface ProjectRepository extends GraphRepository<Project> {

    @Query("MATCH (n:Project ) WHERE n.projectId={0} RETURN n;")
    Project findByProjectId(String projectId);

    @Query("MATCH (a:Person {userId: {1}),(b:Project {projectId: {0}), CREATE (a)-[r:IS_LEAD_ON]->(b); RETURN b")
    Project setLead(String projectId, String userId);

    @Query("MATCH (members:Person)-[:WORKS_ON]->(p:Project {projectId: {0}}) RETURN members")
    ArrayList<Person> getMembers(String projectId);

    @Query("MATCH (lead:Person)-[:IS_LEAD_ON]->(p:Project {projectId: {0}}) RETURN lead")
    Person getLead(String projectId);

}
