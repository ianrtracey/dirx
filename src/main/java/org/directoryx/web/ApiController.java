package org.directoryx.web;

import org.apache.log4j.Logger;
import org.directoryx.Application;
import org.directoryx.domain.Person;
import org.directoryx.service.PersonService;
import org.directoryx.service.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RestController
@RequestMapping("/api/v1/")
public class ApiController extends Controller<Person> {
    static Logger log = Logger.getLogger(Application.class.getName());

    @Autowired
    private PersonService personService;

    @Override
    public Service<Person> getService() {
        return personService;
    }

    @RequestMapping("people/search/{term}")
    public ArrayList<Person> search(@PathVariable(value = "term") String term) {
        return personService.search(term);
    }

    /* Used in filtering AJAX call to get all employees with firstName term */
    @RequestMapping("people/employees/search/{term}")
    public ArrayList<Person> searchEmployees(@PathVariable(value = "term") String term) {
        ArrayList<Person> people = personService.findEmployeeByFirstName(term+".*");
        return people;
    }

    /* Used in filtering AJAX call to get all contractors with firstName term */
    @RequestMapping("people/contractors/search/{term}")
    public ArrayList<Person> searchContractors(@PathVariable(value = "term") String term) {
        ArrayList<Person> contractors = personService.findContractorByFirstName(term + ".*");
        return contractors;
    }

    @RequestMapping("favorite/{userId}")
    public Person favorite(@PathVariable(value = "userId") String userId) {
        Person fav = personService.findByUserId(userId);
        Person ian = personService.findByUserId("iatracey");
        ian.addFavorite(fav);
        personService.savePerson(ian);
        return fav;
    }

    @RequestMapping("unfavorite/{userId}")
    public Person unfavorite(@PathVariable(value = "userId") String userId) {
        Person fav = personService.findByUserId(userId);
        Person ian = personService.findByUserId("iatracey");
        personService.removeFavorite(fav.userId);
        personService.savePerson(ian);
        return fav;
    }



}
