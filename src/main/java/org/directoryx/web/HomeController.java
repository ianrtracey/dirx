package org.directoryx.web;

import org.apache.log4j.Logger;
import org.directoryx.Application;
import org.directoryx.domain.Person;
import org.directoryx.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class HomeController {

    static Logger log = Logger.getLogger(Application.class.getName());


    @Autowired
    PersonService personService;
    @RequestMapping("/")
    public ModelAndView dashboard() {
        ModelAndView mav = new ModelAndView();
        Person ian = personService.findByUserId("iatracey");
        mav.setViewName("dashboard");
        log.info("Found Ian's favorites" + personService.getFavorites(ian.userId));
        mav.addObject("favorites", personService.getFavorites(ian.userId));
        return mav;
    }
}
