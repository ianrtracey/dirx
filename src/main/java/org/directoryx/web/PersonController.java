package org.directoryx.web;

import org.apache.log4j.Logger;
import org.directoryx.Application;
import org.directoryx.domain.Person;
import org.directoryx.domain.PersonWithHash;
import org.directoryx.service.PersonService;
import org.directoryx.service.Service;
import org.neo4j.rest.graphdb.query.CypherTransaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.neo4j.rest.graphdb.query.CypherTransaction.Result;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;


import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping(value = "/people")
public class PersonController extends Controller<Person> {

    static Logger log = Logger.getLogger(Application.class.getName());


    @Autowired
    private PersonService personService;


    @Override
    public Service<Person> getService() {
        return personService;
    }

    @RequestMapping("/person/{userId}")
    public Person getPersonByUserId(@PathVariable(value = "userId") String userId,
                                    final HttpServletResponse response) {
        System.out.println("looking for " + userId + " ...");
        setHeaders(response);
        Person p = personService.findByUserId(userId);
        p.toString();
        return p;
    }


    @RequestMapping("/profile/{userId}")
    public ModelAndView profile(@PathVariable(value = "userId") String userId,
                                final HttpServletResponse response) {
        setHeaders(response);
        Person p = personService.findByUserId(userId);
        return getProfile(p);
    }

    @RequestMapping("/all")
    public Iterable<Person> profile() {
        return personService.findAll();
    }

    @RequestMapping("/search/{term}")
    public ModelAndView search(@PathVariable(value = "term") String term) {
        ModelAndView mav = new ModelAndView();
        ArrayList<Person> people = personService.search(term);
        if(people.size() == 1){
            log.info("Found" + people.get(0).getUserId() + ".");
            return getProfile(people.get(0));
        }
        log.info("Search Results: "+people.size()+" results");

        if (people.size() >= 150) {
            mav.setViewName("search_results_large");
        } else {
            mav.setViewName("search_results");
        }
        mav.addObject("query", term);
        mav.addObject("people", people);

        return mav;
    }

    @RequestMapping("/search/employees/{term}")
    public ModelAndView searchEmployees(@PathVariable(value = "term") String term) {

        ArrayList<Person> employees = personService.findEmployeeByFirstName(term + ".*");
        log.info("Search Results: "+employees.size()+" results");
        ModelAndView mav = new ModelAndView();
        mav.setViewName("search/search_employees");
        mav.addObject("query", term);
        mav.addObject("employees", employees);
        return mav;
    }
    @RequestMapping("/search/contractors/{term}")
    public ModelAndView searchContractors(@PathVariable(value = "term") String term) {

        ArrayList<Person> contractors = personService.findContractorByFirstName(term + ".*");
        log.info("Search Results: "+contractors.size()+" results");
        ModelAndView mav = new ModelAndView();
        mav.setViewName("search/search_contractors");
        mav.addObject("query", term);
        mav.addObject("contractors", contractors);
        return mav;
    }

    private ModelAndView getProfile(Person person) {

        ModelAndView mav = new ModelAndView();

        Collection<PersonWithHash> managers = personService.getManagers(person.getUserId());
        Collection<PersonWithHash> reportees = personService.getReportees(person.getUserId());

        mav.setViewName("profile");
        mav.addObject("managers", managers);
        mav.addObject("person", person);
        mav.addObject("reports", reportees);

        ArrayList<Person> favorites = personService.getFavorites("iatracey");
        if (favorites.contains(person)) {
            Boolean isFavorite = new Boolean(true);
            mav.addObject("isFavorite", isFavorite);
        } else {
            Boolean isFavorite = new Boolean(false);
            mav.addObject("isFavorite", isFavorite);
        }
        return mav;
    }
}
