package org.directoryx.web;

import org.apache.log4j.Logger;
import org.directoryx.Application;
import org.directoryx.domain.Person;
import org.directoryx.domain.Project;
import org.directoryx.service.ProjectService;
import org.directoryx.service.Service;
import org.directoryx.service.PersonService;
import org.directoryx.service.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.directoryx.repository.ProjectRepository;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;


@RestController
@RequestMapping("/project")
public class ProjectController extends Controller<Project> {

    static Logger log = Logger.getLogger(Application.class.getName());


    @Autowired
    private ProjectService projectService;

    @Autowired
    private PersonService personService;


    @Override
    public Service<Project> getService() {
        return projectService;
    }

    @RequestMapping("/details/{projectId}")
    public ModelAndView getProject(@PathVariable(value ="projectId") String projectId) {
        log.info("Attempting to find a project...");
        Project project = projectService.findByProjectId(projectId);
        log.info("PROJECT: "+project.toString());
        ModelAndView mav = new ModelAndView();
        mav.addObject("project",project);
        mav.addObject("members", projectService.getMembers(project));
        mav.addObject("lead", projectService.getLead(project));
        mav.setViewName("project/project");
        return mav;
    }

    @RequestMapping(value="/new", method=RequestMethod.GET)
    public ModelAndView projectNew() {
        ModelAndView mav = new ModelAndView();
        mav.addObject("project", new Project());
        mav.addObject("people", "");
        mav.setViewName("project/new");
        return mav;
    }

    @RequestMapping(value="/create", method=RequestMethod.POST)
    public ModelAndView projectSubmit(HttpServletRequest request,
                                @RequestParam(value="name", required=true) String name,
                                @RequestParam(value="description", required=true) String description,
                                      @RequestParam(value="link", required=true) String link,
                                @RequestParam(value="leadUserId", required=true) String leadUserId,
                                      @RequestParam(value="memberUserIds") String memberUserIds,
                                      @RequestParam(value="startDate") String startDate){

        ModelAndView mav = new ModelAndView();
        try {
            Project project = new Project();
            project.setName(request.getParameter("name"));
            project.setStartDate(request.getParameter("startDate"));
            project.setDescription(request.getParameter("description"));
            project.setLink(request.getParameter("link"));
            project.setProjectId(request.getParameter("name").trim().toLowerCase().replace(' ', '-'));
            mav.addObject("project", project);
            log.info("SETTING THE LEAD...");
            Person l = personService.findByUserId(leadUserId);
            project.lead = l;
            Iterable<String> members = Arrays.asList(memberUserIds.split(","));
            for(String member : members) {
                Person m = personService.findByUserId(member);
                project.addMembers(m);
            }
            projectService.saveProject(project);
        }catch(Exception ex){
            log.error(ex);

        }
        mav.addObject("people", personService.findByFirstName("Caroline" + ".*"));
        mav.setViewName("project/project");
        return mav;
    }

    @RequestMapping("/sample")
    public ModelAndView getProject() {
       	ModelAndView mav = new ModelAndView();
        mav.setViewName("project/project");
        ArrayList<Person> people = personService.findByFirstName("Caroline" + ".*");
        mav.addObject("people", people);
        return mav;
    }
}
