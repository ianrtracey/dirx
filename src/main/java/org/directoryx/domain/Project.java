package org.directoryx.domain;

import org.neo4j.ogm.annotation.Relationship;

import java.util.ArrayList;
import java.util.List;

public class Project extends Entity {

    // @TODO either replace team and projectManager with @RelatedTo object or remove them

    private String name;
    private String description;
    private String link;
    private String startDate;
    private String projectId;

    public Project() { }

    public String getName() {
        return name;
    }

    public void setName(String projectName) {
        this.name = projectName;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getLink() {return link;}

    public void setLink(String link) {this.link = link;}

    public String getProjectId() {return projectId;}

    public void setProjectId(String projectId) {this.projectId = projectId;}

    public void addMembers(Person member) {
        if (members == null) {
            members = new ArrayList<Person>();
        }
        members.add(member);
    }

    @Relationship(type = "IS_LEAD_ON", direction = Relationship.INCOMING)
    public Person lead;

    @Relationship(type = "WORKS_ON", direction = Relationship.INCOMING)
    public ArrayList<Person> members;
}
