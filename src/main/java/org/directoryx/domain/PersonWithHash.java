package org.directoryx.domain;

import org.neo4j.ogm.annotation.NodeEntity;
import org.springframework.data.neo4j.annotation.QueryResult;

import java.util.Map;
import org.neo4j.ogm.annotation.ResultColumn;
import org.springframework.data.neo4j.annotation.QueryResult;


@QueryResult
public class PersonWithHash {

    private Map<String, Object> person;
    private int reportCounts;

    public int getReportCounts() {
        return reportCounts;
    }

    public void setReportCounts(int reportCounts) {
        this.reportCounts = reportCounts;
    }

    public Map<String, Object> getPerson() {
        return person;
    }

    public void setPerson(Map<String, Object> person) {
        this.person = person;
    }
}
