package org.directoryx.domain;

import org.neo4j.ogm.annotation.*;
import org.directoryx.service.PersonService;
import org.springframework.data.neo4j.annotation.QueryResult;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@NodeEntity
public class Person extends Entity {

    public String name;
    public String firstName;
    public String lastName;
    public String userId;
    public String title;
    public String country;
    public String city;
    public String phone;
    public String email;
    public String status;
    public String photo;
    public String org;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOrg() {
        return org;
    }

    public void setOrg(String org) {
        this.org = org;
    }

    public void addFavorite(Person favorite) {
        if (favorites == null) {
            favorites = new ArrayList<Person>();
        }
        favorites.add(favorite);
    }

    public Person() {}

    public Person(String firstName, String lastName, String userId, String title, String country, String city, String phone, String email, String status, String photo, String org) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.userId = userId;
        this.title = title;
        this.country = country;
        this.city = city;
        this.phone = phone;
        this.email = email;
        this.status = status;
        this.photo = photo;
        this.org = org;
    }

    @Relationship(type = "FAVORITES", direction = Relationship.OUTGOING)
    private List<Person> favorites;

}
